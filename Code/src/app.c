/*
 * Simple blinky program as proof of concept
 *
 * Compile with  "xc8 --chip=12F683 blinky.c"
 */

#include <xc.h>
#define _XTAL_FREQ 32000000
#define DELAY_TIME 500

#pragma config FOSC=INTOSC
#pragma config WDTE=OFF
#pragma config PWRTE=OFF
#pragma config MCLRE=ON
#pragma config CP=OFF
#pragma config CPD=OFF
#pragma config BOREN=OFF
#pragma config CLKOUTEN=OFF
#pragma config FCMEN=OFF
#pragma config WRT=OFF
#pragma config PLLEN=ON
#pragma config STVREN=ON
#pragma config BORV=HI
#pragma config LVP=ON

#define LED LATAbits.LATA1

static inline void init(void)
{	
	//set up the oscillator
	OSCCON = 0xF0;
	//Set pin A1 as an output, all others as inputs
	TRISA = 0x00;
	//set all pins as digital
	ANSELA =0x00;
	asm("nop");
}

void main(void)
{
	init();
	//set all outputs low
	LED = 1;
	//blink the LED at 1mHz
	while(1)
	{
		#asm
	top:	bsf LATA,1
			nop
			nop
			bcf LATA,1
			goto top
		#endasm
		//Toggle the GP5 pin
		LED ^= 1;
		//Delay for a set number of ms
		//__delay_us(DELAY_TIME);
	}
}